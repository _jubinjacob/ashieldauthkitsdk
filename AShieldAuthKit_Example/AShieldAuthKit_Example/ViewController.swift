//
//  ViewController.swift
//  AShieldAuthKit_Example
//
//  Created by Jubin Jacob on 18/04/20.
//  Copyright © 2020 ashield. All rights reserved.
//

import UIKit
import AShieldAuthKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        AShieldAuthenticator().authenticate()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerAction(_ sender: Any) {
        self.navigationController?.pushViewController(AShieldSignUpViewController.instantiate(), animated: true)
    }
}

